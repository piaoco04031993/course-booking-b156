//[ACTIVITY]
import {Row, Col, Button, Container} from 'react-bootstrap';


export default function About() {
	return(
		<Row className="p-5 aboutMe">
				<Col xs={12} md={4}>
					<h1 className="my-4"> About Me </h1>
					<h2>Pia Ogawa</h2>
					<h2>Full Stack Web Developer</h2>
					<p>I am a Full Stack Web Developer from Zuitt Coding Bootcamp</p>
					<h2>Contact Me</h2>
					<ul>
						<li>Email: piaoco04031993@gmail.com</li>
						<li>Mobile No. 0926-6929-7272</li>
						<li>Address: Daet, Camarines Norte</li>
					</ul>
				</Col>
		</Row>
	
			
		
	);
};

